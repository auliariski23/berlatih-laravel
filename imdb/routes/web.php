<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

// Template blade
Route::get('/', 'HomeController@home');
Route::get('/register', 'AuthController@form');
Route::get('/welcome', 'AuthController@submit');
Route::post('/welcome', 'AuthController@submit_post');
Route::get('/master', function(){
    return view('admin.master');
});
Route::get('/table', function(){
    return view('admin.table');
});
Route::get('/data-tables', function(){
    return view('admin.data-tables');
});

// CRUD cast
Route::get('/cast/create', 'CastController@create');
Route::post('/cast', 'CastController@store');
Route::get('/cast', 'CastController@index');
Route::get('/cast/{cast_id}', 'CastController@show');
Route::get('/cast/{cast_id}/edit', 'CastController@edit');
Route::put('/cast/{cast_id}', 'CastController@update');
Route::delete('/cast/{cast_id}', 'CastController@destroy');