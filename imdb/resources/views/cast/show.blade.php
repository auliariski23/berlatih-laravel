@extends('admin.master')

@section('title')
    Detail Cast {{$cast->nama}}
@endsection

@section('content')
    <h2>{{$cast->nama}}</h2>
    <p>{{$cast->bio}}</p>
@endsection
