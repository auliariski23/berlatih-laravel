<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Halaman Form</title>
</head>
<body>
    <h1>Buat Account baru!</h1>
    <h2>Sing Up Form</h2>
    <form action="/welcome" method="POST">
        <?php echo csrf_field(); ?>
        <label>First Name:</label> <br><br>
        <input type="text" name="firstname"> <br><br>
        <label>Last Name:</label> <br><br>
        <input type="text" name="lastname"> <br><br>
        <label>Gender:</label><br><br>
        <input type="radio" name="gender"> Male <br>
        <input type="radio" name="gender"> Female<br>
        <input type="radio" name="gender"> Other <br><br>
        <label>Nationality:</label> <br><br>
        <select name="nationality"> 
            <option value="Indonesian">Indonesian</option>
            <option value="Singaporean">Singaporean</option>
            <option value="Malaysian">Malaysian</option>
            <option value="Australian">Australian</option>
        </select> <br><br>
        <label>Languange Spoken:</label><br><br>
        <input type="checkbox" name="languange"> Bahasa Indonesia <br>
        <input type="checkbox" name="languange"> English <br>
        <input type="checkbox" name="languange"> Other <br><br>
        <label>Bio:</label> <br><br>
        <textarea name="bio" cols="30" row="50"></textarea><br>
        <input type="submit" name="submit" value="Sign Up">
    </form>
</body>
</html><?php /**PATH D:\sanbercode - laravel\12. Laravel\latihan\resources\views/register.blade.php ENDPATH**/ ?>